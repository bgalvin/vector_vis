# Vector_Vis #

This is a simple app to visualize deformation vectors produced by image warping software.

+ Mousing over a pixel will show magnitude and direction at that point
+ 'p' pauses playback
+ comma and period increment timestep by 1, holding shift by 10
+ 'v' switches between elastic and greedy deformation types
+ 'b' disables the background image

![alt tag](https://bytebucket.org/bgalvin/vector_vis/raw/29937ad79cb98c6ab3b029b9d4868b77291056b9/example1.bmp)