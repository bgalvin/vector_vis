class myVec {
  
  float x, y, mag, dir, mappedMag, mappedDir; 
  
  myVec (float x_, float y_) {  
    x = x_; 
    y = y_;
    mag = sqrt(x*x+y*y);
    //mappedMag = map(mag, 0, maxVecMag, 0, 255);
    dir = atan(y/x);

    mappedDir = map(dir, -PI/2,PI/2, 0, 255);

  } 
  
  void updateVecMag()
  {
    mappedMag = map(mag, minVecMag, maxVecMag, 0, 255);
  }
 
} 
