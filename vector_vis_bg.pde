
int i = 1;

Table gred_x_table, elas_x_table, gred_y_table, elas_y_table, gred_img_table, elas_img_table;

int  sizeRow   = 255;
int  sizeCol   = 256;
int  timeSteps = 99;

float maxVecMag = 0;
float minVecMag = 1000;

float curMag = 0;
float curDir = 0;

myVec tempGred;
myVec tempElas;

myVec[][][] gred = new myVec[sizeRow][sizeCol][timeSteps];
myVec[][][] elas = new myVec[sizeRow][sizeCol][timeSteps];
float[][][] elas_img = new float[sizeRow][sizeCol][timeSteps];
float[][][] gred_img = new float[sizeRow][sizeCol][timeSteps];

boolean firstTime = true, pause = false, displayGred = true, displayImg = true;



void setup() {

  size(sizeRow*2+20, sizeCol*2+20);
  background(3);

  for (int i = 0; i < timeSteps; i += 1) {

    println("loading("+i+"/99)");

    //  Load in each timesteps vector components
    gred_x_table =   loadTable("gred/gred_" + i + "_x.csv");
    gred_y_table =   loadTable("gred/gred_" + i + "_y.csv");
    gred_img_table = loadTable("gred/gred_"    + i + "_img.csv");

    elas_x_table =    loadTable("elas6/elas_" + i + "_x.csv");
    elas_y_table =    loadTable("elas6/elas_" + i + "_y.csv");
    elas_img_table  = loadTable("elas6/elas_" + i + "_img.csv");



    for (int j = 0; j < sizeRow; j += 1) {
      for (int k = 0; k < sizeCol; k += 1) {
        //println(i+ " " + j+ " " + k) ;

        tempGred = new myVec(gred_x_table.getFloat(j, k), gred_y_table.getFloat(j, k));
        tempElas = new myVec(elas_x_table.getFloat(j, k), elas_y_table.getFloat(j, k));

        maxVecMag = max(maxVecMag, tempGred.mag, tempElas.mag);
        minVecMag = min(minVecMag, tempGred.mag, tempElas.mag);



        gred[j][k][i] = tempGred;
        gred_img[j][k][i] = gred_img_table.getFloat(j, k);

        elas[j][k][i] = tempElas;
        elas_img[j][k][i] = elas_img_table.getFloat(j, k);
      }
    }
  }

  //update the mapped magnitude values
  for (int i = 0; i < timeSteps; i += 1) {
    for (int j = 0; j < sizeRow; j += 1) {
      for (int k = 0; k < sizeCol; k += 1) {
        gred[j][k][i].updateVecMag();
        elas[j][k][i].updateVecMag();
      }
    }
  }
  println(maxVecMag);
  println(minVecMag);
}



void draw() {
  background(0);



  if (displayGred) {
    text("Elastic deformation", 10, 10 + sizeRow + 20 );
  }
  else {
    text("Greedy deformation", 10, 10 + sizeRow + 20);
  }


  text("Timestep:"+i, 10, 10 + sizeRow + 40 );

  text("Magnitude: " +  curMag, 10, 10 + sizeRow + 60);
  text("Direction: " +  curDir, 10, 10 + sizeRow + 80);


  if (!pause) {

    if (i < timeSteps-1) {
      i++;
    }
    else {
      i=1;
    }
  }
  else
  {
    text("PAUSED", 150, 10 + sizeRow + 40 );
  }

  makeImage();
}

int loc;

void makeImage() {
  loadPixels(); 
  for (int j = 0; j < sizeRow; j++) {
    for (int k = 0; k < sizeCol; k++) {

      if (displayGred) {


        if ( displayImg)
        {
          loc = (k+10) + (j+10)*width;
          pixels[loc] =  color(map(elas_img[j][k][i], .129411, .7725, 0, 255), map(elas_img[j][k][i], .129411, .7725, 0, 255)+ int(elas[j][k][i].mag*500), map(elas_img[j][k][i], .129411, .7725, 0, 255)) ;
          loc = (k+10+sizeRow) + (j+10)*width;
          pixels[loc] =  color(map(elas_img[j][k][i], .129411, .7725, 0, 255), map(elas_img[j][k][i], .129411, .7725, 0, 255), map(elas_img[j][k][i], .129411, .7725, 0, 255)+int(elas[j][k][i].mappedDir)) ;
        }
        else
        {
          loc = (k+10) + (j+10)*width;
          pixels[loc] =  color(0, int(elas[j][k][i].mag*500), 0) ;
          loc = (k+10+sizeRow) + (j+10)*width;
          pixels[loc] =  color(0, 0, int(elas[j][k][i].mappedDir)) ;
        }
      }
      else {


        if ( displayImg)
        {
          loc = (k+10) + (j+10)*width;
          pixels[loc] =  color(map(gred_img[j][k][i], .129411, .7725, 0, 255), map(gred_img[j][k][i], .129411, .7725, 0, 255)+ int(gred[j][k][i].mag*500), map(gred_img[j][k][i], .129411, .7725, 0, 255)) ;
          loc = (k+10+sizeRow) + (j+10)*width;
          pixels[loc] =  color(map(gred_img[j][k][i], .129411, .7725, 0, 255), map(gred_img[j][k][i], .129411, .7725, 0, 255), map(gred_img[j][k][i], .129411, .7725, 0, 255)+int(gred[j][k][i].mappedDir)) ;
        }
        else
        {
          loc = (k+10) + (j+10)*width;
          pixels[loc] =  color(0, int(gred[j][k][i].mag*500), 0) ;
          loc = (k+10+sizeRow) + (j+10)*width;
          pixels[loc] =  color(0, 0, int(gred[j][k][i].mappedDir)) ;
        }
      }

      loc = (k+11+sizeRow) + (j+9+sizeCol)*width;
      int value = int(abs( map(gred_img[j][k][i], .129411, .7725, 0, 255)- map(elas_img[j][k][i], .129411, .7725, 0, 255) ));
      pixels[loc] = color(value, value, value);
    }
  }



  updatePixels();

  stroke(255);
  text("Vector Magnitude", 15, 25 );
  text("Vector Direction", sizeCol+15, 25 );
  text("Difference Image", sizeCol+15, 25 +sizeRow );

  text("'p' pauses", 10, sizeRow+sizeRow/2+20 );
  text("',' and '.' change timestep by 1", 10, sizeRow+sizeRow/2 +40);
  text("'<' and '>' change timestep by 10", 10, sizeRow+sizeRow/2+60 );
  text("'v' changes deformation type ", 10, sizeRow+sizeRow/2+80 );
  text("'b' disables background image ", 10, sizeRow+sizeRow/2+100 );

  if (mouseX > 10 && mouseX < 10+sizeRow && mouseY > 10 && mouseY < 10+sizeCol) {
    if (displayGred) {
      curMag = (elas[mouseX-10][mouseY-10][i].mag); 
      curDir = (elas[mouseX-10][mouseY-10][i].dir);
    }
    else
    {
      curMag = (gred[mouseX-10][mouseY-10][i].mag); 
      curDir = (gred[mouseX-10][mouseY-10][i].dir);
    }
  }
  if (mouseX > 10+sizeRow && mouseX < 10+sizeRow*2 && mouseY > 10 && mouseY < 10+sizeCol) {
    if (displayGred) {
      curMag = (elas[mouseX-10-sizeRow][mouseY-10][i].mag); 
      curDir = (elas[mouseX-10-sizeRow][mouseY-10][i].dir);
    }
    else
    {
      curMag = (gred[mouseX-10-sizeRow][mouseY-10][i].mag); 
      curDir = (gred[mouseX-10-sizeRow][mouseY-10][i].dir);
    }
  }
}


void keyPressed() {

  if (key == 'p') {
    pause = !pause;
    println("pause = "+pause);
  } 

  if (key == ',') {
    if (i > 0) { 
      i--;
    }
    else {
      i = timeSteps-1;
    }
  } 

  if (key == '.') {
    if (i < timeSteps-1) { 
      i++;
    }
    else {
      i = 0;
    }
  }

  if (key == '<') {
    if (i > 10) { 
      i = i-10;
    }
    else {
      i = timeSteps-1;
    }
  } 

  if (key == '>') {
    if (i < timeSteps-10) { 
      i= i +10;
    }
    else {
      i = 0;
    }
  }


  if (key == 'v') {
    displayGred = !displayGred;
    if (displayGred) {
      println("Showing elastic deformation");
    }
    else {
      println("Showing greedy deformation");
    }
  } 
  if (key == 'b') {
    displayImg = !displayImg;
  }
}


//void mouseMoved() {
//
//  // top left
//  if (mouseX > 10 && mouseX < 10+sizeRow && mouseY > 10 && mouseY < 10+sizeCol) {
//    if (displayGred) {
//      curMag = (elas[mouseX-10][mouseY-10][i].mag); 
//      curDir = (elas[mouseX-10][mouseY-10][i].dir);
//    }
//    else
//    {
//      curMag = (gred[mouseX-10][mouseY-10][i].mag); 
//      curDir = (gred[mouseX-10][mouseY-10][i].dir);
//    }
//  }
//  if (mouseX > 10+sizeRow && mouseX < 10+sizeRow*2 && mouseY > 10 && mouseY < 10+sizeCol) {
//    if (displayGred) {
//      curMag = (elas[mouseX-10-sizeRow][mouseY-10][i].mag); 
//      curDir = (elas[mouseX-10-sizeRow][mouseY-10][i].dir);
//    }
//    else
//    {
//      curMag = (gred[mouseX-10-sizeRow][mouseY-10][i].mag); 
//      curDir = (gred[mouseX-10-sizeRow][mouseY-10][i].dir);
//    }
//  }
//}

